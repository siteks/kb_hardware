* MAX4487 MACROMODEL
* ----------------------------
* Revision 0. 3/2004
* ----------------------------
* The MAX4487 low cost general purpose opamp operates from a single +2.7V to 5.5V
* Supply, feature ground sensing inputs and rail to rail outputs and are unity 
* gain stable
* ----------------------------
* Connections
*       1 = Noninverting input of the amplifier
*       2 = Negative power supply
*       3 = Inverting input of the amplifier
*       4 = Amplifier output
*       5 = Positive power supply   
*-----------------------------
*$
**********************************
.SUBCKT MAX4487 1 2 3 4 5
 XOPAMP 1 2 3 4 5 MAX4487_S
.ENDS
**********************************

**********************************
.SUBCKT MAX4487_S 17 18 15 42 10
**********************************
*INPUT STAGE
VS1 10 11 0V
IBIAS 11 12 363.76U 
M1 13 16 12 11 MOSFET
M2 14 15 12 11 MOSFET
VOS 17 16 300U   
RD1 13 18 1K    
RD2 14 18 1K
C1 13 14 8.2P    
DIN1 16 11 DA
CIN1 16 100 2P
CIN2 15 100 2P   
DIN2 18 16 DA
DIN3 15 11 DA
DIN4 18 15 DA
FSUP 18 10 VS1 1 
**************
*INPUT BIAS CURRENT 
IBIAS1 12 16 0.1P
IBIAS2 12 15 0.1P 
**************
*MAXIMUM INPUT COMMON MODE VOLTAGE LIMIT
DIL 12 80 DY
RIL 80 82 10K 
VIL 81 82 1020MV
EIL 81 18 10 18 1
**************
*MINIMUM SUPPLY VOLTAGE LIMIT 
VVL 85 18 2.7V
VIS4 85 86 0V
RVL 86 87 10K
DVL3 87 88 DY
EVL 88 18 10 18 1
DVL4 18 12 DY
DVL5 12 84 DY
FVL 84 18 VIS4 4 
**************************************************
*GAIN STAGE 
GA 25 100 14 13 439.82U  
RO1 25 100 2.274K
GB 26 100 25 100 1.0
RO2 26 100 79.433K
EF 27 100 26 100 1   
RLF 27 100 1MEG 
CC 25 26 8P        
***************
*VOLTAGE LIMITING
DP1 26 151 DY           
EP1 151 153 10 18 0.5
EP3 153 155 26 199 1
HCOMP1 100 155 VIS2 83.3
DP2 152 26 DY
EP2 154 152 10 18 0.5
EP4 156 154 199 26 1
HCOMP2 100 156 VIS2 83.3
**************************************************
*CMRR (CMRR FREQUENCY RESPONSE NOT ADDED)
GCM 100 25 18 12 247.29N 
**************************************************
*PSRR (PSRR FREQUENCY RESPONSE NOT ADDED)
GPS 100 25 10 18 320.0N 
**************************************************
****************
RO3 27 199 100
*CURRENT LIMITING AT TWO LEVELS FOR SINK AND SOURCE
D1 30 199 DY
D2 199 28 DY
D3 29 28 DY
D4 30 29 DY
ILIM1 28 30 27M
RILIM 28 30 10K
***********
VIS1 34 29 0V 
FSW 100 36 VIS1 1 
D9 100 36 DX    
D10 36 35 DX
D11 35 37 DY
VLIM1 37 100 1V 
R1 35 100 1MEG
CX1 35 100 0.050P
GIL 28 30 35 100 6M  
**************************************************
VIS2 34 42 0V    
**************************************************
*INTERNAL GROUND AT VDD/2
EG1 100 18 10 18 0.5
**************************************************
*SUPPLY CURRENT MODEL
*STANDBY CURRENT
ISUP 10 18 2.2M
DISUP 10 90 DY
RISUP 90 91 20MEG
VISUP 91 18 2.5V
***************
*LOAD CURRENT
DSUP 18 10 DX        
FIS1 100 60 VIS2 1
D17 100 60 DX
D18 60 61 DX
VIS3 100 61 0V
FSUP1 18 10 VIS3 1   
**************************************************
.MODEL DA D(IS=100E-14 RS=0.5k)
.MODEL MOSFET PMOS(VTO=-0.2 KP=27.5E-4)
.MODEL DX D(IS=100E-14)
.MODEL DZ D(N=10M)
.MODEL DY D(IS=100E-14 N=0.1M)
**************************************************
.ENDS
*$
