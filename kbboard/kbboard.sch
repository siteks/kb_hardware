EESchema Schematic File Version 2
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:mycomponents
LIBS:power
LIBS:kbboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1400 1200 1500 1200
U 54E131DF
F0 "Power supply" 60
F1 "power_supply.sch" 60
$EndSheet
$Sheet
S 1400 3100 1500 1200
U 54E0F1DD
F0 "Communication" 60
F1 "communication.sch" 60
$EndSheet
$Sheet
S 1400 4900 1500 1200
U 54E52325
F0 "CPU" 60
F1 "cpu.sch" 60
$EndSheet
$EndSCHEMATC
