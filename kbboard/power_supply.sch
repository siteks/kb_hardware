EESchema Schematic File Version 2
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:mycomponents
LIBS:power
LIBS:kbboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TC1016_3V0 U1
U 1 1 54E1525B
P 2900 5200
AR Path="/54E131DF/54E1525B" Ref="U1"  Part="1" 
AR Path="/54E15959/54E1525B" Ref="U?"  Part="1" 
F 0 "U1" H 3050 4700 60  0000 C CNN
F 1 "TC1016_3V0" H 2900 5250 60  0000 C CNN
F 2 "" H 2900 5400 60  0000 C CNN
F 3 "" H 2900 5400 60  0000 C CNN
	1    2900 5200
	1    0    0    -1  
$EndComp
$Comp
L Cpol C7
U 1 1 54E15262
P 3600 5800
AR Path="/54E131DF/54E15262" Ref="C7"  Part="1" 
AR Path="/54E15959/54E15262" Ref="C?"  Part="1" 
F 0 "C7" H 3600 5900 40  0000 L CNN
F 1 "2u2" H 3606 5715 40  0000 L CNN
F 2 "" H 3638 5650 30  0000 C CNN
F 3 "" H 3600 5800 60  0000 C CNN
	1    3600 5800
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 54E15269
P 3900 5800
AR Path="/54E131DF/54E15269" Ref="C10"  Part="1" 
AR Path="/54E15959/54E15269" Ref="C?"  Part="1" 
F 0 "C10" H 3950 5900 50  0000 L CNN
F 1 "10n" H 3950 5700 50  0000 L CNN
F 2 "" H 3938 5650 30  0000 C CNN
F 3 "" H 3900 5800 60  0000 C CNN
	1    3900 5800
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 54E15270
P 4200 5800
AR Path="/54E131DF/54E15270" Ref="C13"  Part="1" 
AR Path="/54E15959/54E15270" Ref="C?"  Part="1" 
F 0 "C13" H 4250 5900 50  0000 L CNN
F 1 "10n" H 4250 5700 50  0000 L CNN
F 2 "" H 4238 5650 30  0000 C CNN
F 3 "" H 4200 5800 60  0000 C CNN
	1    4200 5800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR6
U 1 1 54E15277
P 2900 6300
AR Path="/54E131DF/54E15277" Ref="#PWR6"  Part="1" 
AR Path="/54E15959/54E15277" Ref="#PWR?"  Part="1" 
F 0 "#PWR6" H 2900 6050 60  0001 C CNN
F 1 "GND" H 2900 6150 60  0000 C CNN
F 2 "" H 2900 6300 60  0000 C CNN
F 3 "" H 2900 6300 60  0000 C CNN
	1    2900 6300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR8
U 1 1 54E1527D
P 3600 6300
AR Path="/54E131DF/54E1527D" Ref="#PWR8"  Part="1" 
AR Path="/54E15959/54E1527D" Ref="#PWR?"  Part="1" 
F 0 "#PWR8" H 3600 6050 60  0001 C CNN
F 1 "GND" H 3600 6150 60  0000 C CNN
F 2 "" H 3600 6300 60  0000 C CNN
F 3 "" H 3600 6300 60  0000 C CNN
	1    3600 6300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR10
U 1 1 54E15283
P 3900 6300
AR Path="/54E131DF/54E15283" Ref="#PWR10"  Part="1" 
AR Path="/54E15959/54E15283" Ref="#PWR?"  Part="1" 
F 0 "#PWR10" H 3900 6050 60  0001 C CNN
F 1 "GND" H 3900 6150 60  0000 C CNN
F 2 "" H 3900 6300 60  0000 C CNN
F 3 "" H 3900 6300 60  0000 C CNN
	1    3900 6300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR13
U 1 1 54E15289
P 4200 6300
AR Path="/54E131DF/54E15289" Ref="#PWR13"  Part="1" 
AR Path="/54E15959/54E15289" Ref="#PWR?"  Part="1" 
F 0 "#PWR13" H 4200 6050 60  0001 C CNN
F 1 "GND" H 4200 6150 60  0000 C CNN
F 2 "" H 4200 6300 60  0000 C CNN
F 3 "" H 4200 6300 60  0000 C CNN
	1    4200 6300
	1    0    0    -1  
$EndComp
$Comp
L +3V0 #PWR11
U 1 1 54E1528F
P 4050 5100
AR Path="/54E131DF/54E1528F" Ref="#PWR11"  Part="1" 
AR Path="/54E15959/54E1528F" Ref="#PWR?"  Part="1" 
F 0 "#PWR11" H 4050 4950 60  0001 C CNN
F 1 "+3V0" H 4050 5240 60  0000 C CNN
F 2 "" H 4050 5100 60  0000 C CNN
F 3 "" H 4050 5100 60  0000 C CNN
	1    4050 5100
	1    0    0    -1  
$EndComp
$Comp
L LM3658 U4
U 1 1 54E15295
P 2800 1750
AR Path="/54E131DF/54E15295" Ref="U4"  Part="1" 
AR Path="/54E15959/54E15295" Ref="U?"  Part="1" 
F 0 "U4" H 3000 1050 60  0000 C CNN
F 1 "LM3658" H 2800 1750 60  0000 C CNN
F 2 "" H 2800 1750 60  0000 C CNN
F 3 "" H 2800 1750 60  0000 C CNN
	1    2800 1750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR5
U 1 1 54E1529C
P 2800 3000
AR Path="/54E131DF/54E1529C" Ref="#PWR5"  Part="1" 
AR Path="/54E15959/54E1529C" Ref="#PWR?"  Part="1" 
F 0 "#PWR5" H 2800 2750 60  0001 C CNN
F 1 "GND" H 2800 2850 60  0000 C CNN
F 2 "" H 2800 3000 60  0000 C CNN
F 3 "" H 2800 3000 60  0000 C CNN
	1    2800 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR2
U 1 1 54E152A2
P 2150 3000
AR Path="/54E131DF/54E152A2" Ref="#PWR2"  Part="1" 
AR Path="/54E15959/54E152A2" Ref="#PWR?"  Part="1" 
F 0 "#PWR2" H 2150 2750 60  0001 C CNN
F 1 "GND" H 2150 2850 60  0000 C CNN
F 2 "" H 2150 3000 60  0000 C CNN
F 3 "" H 2150 3000 60  0000 C CNN
	1    2150 3000
	1    0    0    -1  
$EndComp
$Comp
L Cpol C17
U 1 1 54E152A8
P 1650 2300
AR Path="/54E131DF/54E152A8" Ref="C17"  Part="1" 
AR Path="/54E15959/54E152A8" Ref="C?"  Part="1" 
F 0 "C17" H 1650 2400 40  0000 L CNN
F 1 "2u2" H 1656 2215 40  0000 L CNN
F 2 "" H 1688 2150 30  0000 C CNN
F 3 "" H 1650 2300 60  0000 C CNN
	1    1650 2300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR1
U 1 1 54E152AF
P 1650 3000
AR Path="/54E131DF/54E152AF" Ref="#PWR1"  Part="1" 
AR Path="/54E15959/54E152AF" Ref="#PWR?"  Part="1" 
F 0 "#PWR1" H 1650 2750 60  0001 C CNN
F 1 "GND" H 1650 2850 60  0000 C CNN
F 2 "" H 1650 3000 60  0000 C CNN
F 3 "" H 1650 3000 60  0000 C CNN
	1    1650 3000
	1    0    0    -1  
$EndComp
$Comp
L DIODE D1
U 1 1 54E152B5
P 1350 1850
AR Path="/54E131DF/54E152B5" Ref="D1"  Part="1" 
AR Path="/54E15959/54E152B5" Ref="D?"  Part="1" 
F 0 "D1" H 1350 1950 50  0000 C CNN
F 1 "DIODE" H 1350 1750 50  0000 C CNN
F 2 "" H 1350 1850 60  0000 C CNN
F 3 "" H 1350 1850 60  0000 C CNN
	1    1350 1850
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P1
U 1 1 54E152BC
P 850 1950
AR Path="/54E131DF/54E152BC" Ref="P1"  Part="1" 
AR Path="/54E15959/54E152BC" Ref="P?"  Part="1" 
F 0 "P1" H 850 2150 50  0000 C CNN
F 1 "Legs" V 950 1950 50  0000 C CNN
F 2 "" H 850 1950 60  0000 C CNN
F 3 "" H 850 1950 60  0000 C CNN
	1    850  1950
	-1   0    0    1   
$EndComp
$Comp
L R R36
U 1 1 54E152C3
P 3750 2400
AR Path="/54E131DF/54E152C3" Ref="R36"  Part="1" 
AR Path="/54E15959/54E152C3" Ref="R?"  Part="1" 
F 0 "R36" V 3830 2400 50  0000 C CNN
F 1 "25k" V 3757 2401 50  0000 C CNN
F 2 "" V 3680 2400 30  0000 C CNN
F 3 "" H 3750 2400 30  0000 C CNN
	1    3750 2400
	1    0    0    -1  
$EndComp
$Comp
L R R34
U 1 1 54E152CA
P 4100 2400
AR Path="/54E131DF/54E152CA" Ref="R34"  Part="1" 
AR Path="/54E15959/54E152CA" Ref="R?"  Part="1" 
F 0 "R34" V 4180 2400 50  0000 C CNN
F 1 "10k" V 4107 2401 50  0000 C CNN
F 2 "" V 4030 2400 30  0000 C CNN
F 3 "" H 4100 2400 30  0000 C CNN
	1    4100 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR9
U 1 1 54E152D1
P 3750 3000
AR Path="/54E131DF/54E152D1" Ref="#PWR9"  Part="1" 
AR Path="/54E15959/54E152D1" Ref="#PWR?"  Part="1" 
F 0 "#PWR9" H 3750 2750 60  0001 C CNN
F 1 "GND" H 3750 2850 60  0000 C CNN
F 2 "" H 3750 3000 60  0000 C CNN
F 3 "" H 3750 3000 60  0000 C CNN
	1    3750 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR12
U 1 1 54E152D7
P 4100 3000
AR Path="/54E131DF/54E152D7" Ref="#PWR12"  Part="1" 
AR Path="/54E15959/54E152D7" Ref="#PWR?"  Part="1" 
F 0 "#PWR12" H 4100 2750 60  0001 C CNN
F 1 "GND" H 4100 2850 60  0000 C CNN
F 2 "" H 4100 3000 60  0000 C CNN
F 3 "" H 4100 3000 60  0000 C CNN
	1    4100 3000
	1    0    0    -1  
$EndComp
Text Notes 2900 1400 0    60   ~ 0
Ichg = Kiset/Riset
Text Notes 2850 1500 0    60   ~ 0
= 2500/25000 = 0.1A
$Comp
L BATTERY BT1
U 1 1 54E152DF
P 4550 2200
AR Path="/54E131DF/54E152DF" Ref="BT1"  Part="1" 
AR Path="/54E15959/54E152DF" Ref="BT?"  Part="1" 
F 0 "BT1" H 4550 2400 50  0000 C CNN
F 1 "BATTERY" H 4550 2010 50  0000 C CNN
F 2 "" H 4550 2200 60  0000 C CNN
F 3 "" H 4550 2200 60  0000 C CNN
	1    4550 2200
	0    1    1    0   
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 54E152E6
P 4750 2600
AR Path="/54E131DF/54E152E6" Ref="P2"  Part="1" 
AR Path="/54E15959/54E152E6" Ref="P?"  Part="1" 
F 0 "P2" H 4750 2750 50  0000 C CNN
F 1 "CONN_01X02" V 4850 2600 50  0000 C CNN
F 2 "" H 4750 2600 60  0000 C CNN
F 3 "" H 4750 2600 60  0000 C CNN
	1    4750 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR14
U 1 1 54E152ED
P 4550 3000
AR Path="/54E131DF/54E152ED" Ref="#PWR14"  Part="1" 
AR Path="/54E15959/54E152ED" Ref="#PWR?"  Part="1" 
F 0 "#PWR14" H 4550 2750 60  0001 C CNN
F 1 "GND" H 4550 2850 60  0000 C CNN
F 2 "" H 4550 3000 60  0000 C CNN
F 3 "" H 4550 3000 60  0000 C CNN
	1    4550 3000
	1    0    0    -1  
$EndComp
$Comp
L Cpol C1
U 1 1 54E152F3
P 2150 5800
AR Path="/54E131DF/54E152F3" Ref="C1"  Part="1" 
AR Path="/54E15959/54E152F3" Ref="C?"  Part="1" 
F 0 "C1" H 2150 5900 40  0000 L CNN
F 1 "2u2" H 2156 5715 40  0000 L CNN
F 2 "" H 2188 5650 30  0000 C CNN
F 3 "" H 2150 5800 60  0000 C CNN
	1    2150 5800
	1    0    0    -1  
$EndComp
$Comp
L +BATT #PWR15
U 1 1 54E152FA
P 5200 1700
AR Path="/54E131DF/54E152FA" Ref="#PWR15"  Part="1" 
AR Path="/54E15959/54E152FA" Ref="#PWR?"  Part="1" 
F 0 "#PWR15" H 5200 1550 60  0001 C CNN
F 1 "+BATT" H 5200 1840 60  0000 C CNN
F 2 "" H 5200 1700 60  0000 C CNN
F 3 "" H 5200 1700 60  0000 C CNN
	1    5200 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 5250 3600 5250
Wire Wire Line
	3600 5250 3750 5250
Wire Wire Line
	3750 5250 3900 5250
Wire Wire Line
	3900 5250 4050 5250
Wire Wire Line
	4050 5250 4200 5250
Wire Wire Line
	4200 5250 4450 5250
Wire Wire Line
	3600 5250 3600 5600
Wire Wire Line
	3900 5250 3900 5600
Connection ~ 3600 5250
Wire Wire Line
	4050 5100 4050 5250
Connection ~ 3900 5250
Wire Wire Line
	2900 5850 2900 6300
Wire Wire Line
	3600 6000 3600 6300
Wire Wire Line
	3900 6000 3900 6300
Wire Wire Line
	4200 6000 4200 6300
Connection ~ 4050 5250
Wire Wire Line
	2800 2600 2800 3000
Wire Wire Line
	2250 2250 2150 2250
Wire Wire Line
	2150 2050 2150 2250
Wire Wire Line
	2150 2250 2150 3000
Wire Wire Line
	2250 2050 2150 2050
Connection ~ 2150 2250
Wire Wire Line
	2250 2150 2050 2150
Wire Wire Line
	2050 2150 2050 1850
Wire Wire Line
	1550 1850 1650 1850
Wire Wire Line
	1650 1850 1850 1850
Wire Wire Line
	1850 1850 2050 1850
Wire Wire Line
	2050 1850 2250 1850
Wire Wire Line
	1650 1850 1650 2100
Connection ~ 2050 1850
Wire Wire Line
	1650 2500 1650 3000
Wire Wire Line
	1050 1850 1150 1850
Wire Wire Line
	1050 1950 1150 1950
Wire Wire Line
	1150 1850 1150 1950
Wire Wire Line
	1150 1950 1150 2050
Wire Wire Line
	1150 2050 1050 2050
Connection ~ 1150 1950
Connection ~ 1650 1850
Wire Wire Line
	3750 2650 3750 3000
Wire Wire Line
	4100 2650 4100 3000
Wire Wire Line
	3350 2050 3750 2050
Wire Wire Line
	3750 2050 3750 2150
Wire Wire Line
	3350 1950 4100 1950
Wire Wire Line
	4100 1950 4100 2150
Wire Wire Line
	4550 2500 4550 2550
Wire Wire Line
	4550 2650 4550 3000
Wire Wire Line
	3350 1850 4200 1850
Wire Wire Line
	4200 1850 4550 1850
Wire Wire Line
	4550 1850 5200 1850
Wire Wire Line
	4550 1850 4550 1900
$Comp
L R R32
U 1 1 54E15326
P 5200 2150
AR Path="/54E131DF/54E15326" Ref="R32"  Part="1" 
AR Path="/54E15959/54E15326" Ref="R?"  Part="1" 
F 0 "R32" V 5280 2150 50  0000 C CNN
F 1 "2M" V 5207 2151 50  0000 C CNN
F 2 "" V 5130 2150 30  0000 C CNN
F 3 "" H 5200 2150 30  0000 C CNN
	1    5200 2150
	1    0    0    -1  
$EndComp
$Comp
L R R31
U 1 1 54E1532D
P 5200 2700
AR Path="/54E131DF/54E1532D" Ref="R31"  Part="1" 
AR Path="/54E15959/54E1532D" Ref="R?"  Part="1" 
F 0 "R31" V 5280 2700 50  0000 C CNN
F 1 "2M" V 5207 2701 50  0000 C CNN
F 2 "" V 5130 2700 30  0000 C CNN
F 3 "" H 5200 2700 30  0000 C CNN
	1    5200 2700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR16
U 1 1 54E15334
P 5200 3000
AR Path="/54E131DF/54E15334" Ref="#PWR16"  Part="1" 
AR Path="/54E15959/54E15334" Ref="#PWR?"  Part="1" 
F 0 "#PWR16" H 5200 2750 60  0001 C CNN
F 1 "GND" H 5200 2850 60  0000 C CNN
F 2 "" H 5200 3000 60  0000 C CNN
F 3 "" H 5200 3000 60  0000 C CNN
	1    5200 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3000 5200 2950
Wire Wire Line
	5200 2450 5200 2400
Connection ~ 4550 1850
Wire Wire Line
	5200 1700 5200 1850
Wire Wire Line
	5200 1850 5200 1900
Connection ~ 5200 1850
Text GLabel 5500 2450 2    60   Output ~ 0
BSENSE
Wire Wire Line
	5200 2450 5500 2450
$Comp
L PWR_FLAG #FLG3
U 1 1 54E15341
P 4200 1800
AR Path="/54E131DF/54E15341" Ref="#FLG3"  Part="1" 
AR Path="/54E15959/54E15341" Ref="#FLG?"  Part="1" 
F 0 "#FLG3" H 4200 1895 30  0001 C CNN
F 1 "PWR_FLAG" H 4200 1980 30  0000 C CNN
F 2 "" H 4200 1800 60  0000 C CNN
F 3 "" H 4200 1800 60  0000 C CNN
	1    4200 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 1800 4200 1850
Connection ~ 4200 1850
$Comp
L PWR_FLAG #FLG1
U 1 1 54E15349
P 1850 1800
AR Path="/54E131DF/54E15349" Ref="#FLG1"  Part="1" 
AR Path="/54E15959/54E15349" Ref="#FLG?"  Part="1" 
F 0 "#FLG1" H 1850 1895 30  0001 C CNN
F 1 "PWR_FLAG" H 1850 1980 30  0000 C CNN
F 2 "" H 1850 1800 60  0000 C CNN
F 3 "" H 1850 1800 60  0000 C CNN
	1    1850 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 1800 1850 1850
Connection ~ 1850 1850
$Comp
L PWR_FLAG #FLG2
U 1 1 54E15351
P 3750 5200
AR Path="/54E131DF/54E15351" Ref="#FLG2"  Part="1" 
AR Path="/54E15959/54E15351" Ref="#FLG?"  Part="1" 
F 0 "#FLG2" H 3750 5295 30  0001 C CNN
F 1 "PWR_FLAG" H 3750 5380 30  0000 C CNN
F 2 "" H 3750 5200 60  0000 C CNN
F 3 "" H 3750 5200 60  0000 C CNN
	1    3750 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5200 3750 5250
Connection ~ 3750 5250
Wire Wire Line
	2150 5250 2450 5250
$Comp
L +BATT #PWR3
U 1 1 54E1535A
P 2150 5100
AR Path="/54E131DF/54E1535A" Ref="#PWR3"  Part="1" 
AR Path="/54E15959/54E1535A" Ref="#PWR?"  Part="1" 
F 0 "#PWR3" H 2150 4950 60  0001 C CNN
F 1 "+BATT" H 2150 5240 60  0000 C CNN
F 2 "" H 2150 5100 60  0000 C CNN
F 3 "" H 2150 5100 60  0000 C CNN
	1    2150 5100
	1    0    0    -1  
$EndComp
Connection ~ 2150 5250
$Comp
L GND #PWR4
U 1 1 54E15361
P 2150 6300
AR Path="/54E131DF/54E15361" Ref="#PWR4"  Part="1" 
AR Path="/54E15959/54E15361" Ref="#PWR?"  Part="1" 
F 0 "#PWR4" H 2150 6050 60  0001 C CNN
F 1 "GND" H 2150 6150 60  0000 C CNN
F 2 "" H 2150 6300 60  0000 C CNN
F 3 "" H 2150 6300 60  0000 C CNN
	1    2150 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 5100 2150 5250
Wire Wire Line
	2150 5250 2150 5600
Wire Wire Line
	2150 6000 2150 6300
Wire Wire Line
	2450 5250 2450 5400
$Comp
L XC6210B302M U2
U 1 1 54E1536A
P 8200 1700
AR Path="/54E131DF/54E1536A" Ref="U2"  Part="1" 
AR Path="/54E15959/54E1536A" Ref="U?"  Part="1" 
F 0 "U2" H 8400 1200 60  0000 C CNN
F 1 "XC6210B302M" H 8200 1650 60  0000 C CNN
F 2 "" H 8200 1700 60  0000 C CNN
F 3 "" H 8200 1700 60  0000 C CNN
	1    8200 1700
	1    0    0    -1  
$EndComp
$Comp
L Cpol C3
U 1 1 54E15371
P 7300 2400
AR Path="/54E131DF/54E15371" Ref="C3"  Part="1" 
AR Path="/54E15959/54E15371" Ref="C?"  Part="1" 
F 0 "C3" H 7300 2500 40  0000 L CNN
F 1 "2u2" H 7306 2315 40  0000 L CNN
F 2 "" H 7338 2250 30  0000 C CNN
F 3 "" H 7300 2400 60  0000 C CNN
	1    7300 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 1700 7300 1850
Wire Wire Line
	7300 1850 7300 2200
Wire Wire Line
	7300 1850 7700 1850
$Comp
L Cpol C8
U 1 1 54E1537A
P 9000 2400
AR Path="/54E131DF/54E1537A" Ref="C8"  Part="1" 
AR Path="/54E15959/54E1537A" Ref="C?"  Part="1" 
F 0 "C8" H 9000 2500 40  0000 L CNN
F 1 "2u2" H 9006 2315 40  0000 L CNN
F 2 "" H 9038 2250 30  0000 C CNN
F 3 "" H 9000 2400 60  0000 C CNN
	1    9000 2400
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 54E15381
P 9300 2400
AR Path="/54E131DF/54E15381" Ref="C11"  Part="1" 
AR Path="/54E15959/54E15381" Ref="C?"  Part="1" 
F 0 "C11" H 9350 2500 50  0000 L CNN
F 1 "10n" H 9350 2300 50  0000 L CNN
F 2 "" H 9338 2250 30  0000 C CNN
F 3 "" H 9300 2400 60  0000 C CNN
	1    9300 2400
	1    0    0    -1  
$EndComp
$Comp
L Cpol C14
U 1 1 54E15388
P 9600 2400
AR Path="/54E131DF/54E15388" Ref="C14"  Part="1" 
AR Path="/54E15959/54E15388" Ref="C?"  Part="1" 
F 0 "C14" H 9600 2500 40  0000 L CNN
F 1 "2u2" H 9606 2315 40  0000 L CNN
F 2 "" H 9638 2250 30  0000 C CNN
F 3 "" H 9600 2400 60  0000 C CNN
	1    9600 2400
	1    0    0    -1  
$EndComp
$Comp
L C C15
U 1 1 54E1538F
P 9900 2400
AR Path="/54E131DF/54E1538F" Ref="C15"  Part="1" 
AR Path="/54E15959/54E1538F" Ref="C?"  Part="1" 
F 0 "C15" H 9950 2500 50  0000 L CNN
F 1 "10n" H 9950 2300 50  0000 L CNN
F 2 "" H 9938 2250 30  0000 C CNN
F 3 "" H 9900 2400 60  0000 C CNN
	1    9900 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 1850 9000 1850
Wire Wire Line
	9000 1850 9300 1850
Wire Wire Line
	9300 1850 9450 1850
Wire Wire Line
	9450 1850 9600 1850
Wire Wire Line
	9600 1850 9900 1850
Wire Wire Line
	9900 1700 9900 1850
Wire Wire Line
	9900 1850 9900 2200
Wire Wire Line
	9600 2200 9600 1850
Connection ~ 9600 1850
Wire Wire Line
	9300 2200 9300 1850
Connection ~ 9300 1850
Wire Wire Line
	9000 2200 9000 1850
Connection ~ 9000 1850
$Comp
L GND #PWR26
U 1 1 54E1539E
P 9000 3000
AR Path="/54E131DF/54E1539E" Ref="#PWR26"  Part="1" 
AR Path="/54E15959/54E1539E" Ref="#PWR?"  Part="1" 
F 0 "#PWR26" H 9000 2750 60  0001 C CNN
F 1 "GND" H 9000 2850 60  0000 C CNN
F 2 "" H 9000 3000 60  0000 C CNN
F 3 "" H 9000 3000 60  0000 C CNN
	1    9000 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR28
U 1 1 54E153A4
P 9300 3000
AR Path="/54E131DF/54E153A4" Ref="#PWR28"  Part="1" 
AR Path="/54E15959/54E153A4" Ref="#PWR?"  Part="1" 
F 0 "#PWR28" H 9300 2750 60  0001 C CNN
F 1 "GND" H 9300 2850 60  0000 C CNN
F 2 "" H 9300 3000 60  0000 C CNN
F 3 "" H 9300 3000 60  0000 C CNN
	1    9300 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR30
U 1 1 54E153AA
P 9600 3000
AR Path="/54E131DF/54E153AA" Ref="#PWR30"  Part="1" 
AR Path="/54E15959/54E153AA" Ref="#PWR?"  Part="1" 
F 0 "#PWR30" H 9600 2750 60  0001 C CNN
F 1 "GND" H 9600 2850 60  0000 C CNN
F 2 "" H 9600 3000 60  0000 C CNN
F 3 "" H 9600 3000 60  0000 C CNN
	1    9600 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR34
U 1 1 54E153B0
P 9900 3000
AR Path="/54E131DF/54E153B0" Ref="#PWR34"  Part="1" 
AR Path="/54E15959/54E153B0" Ref="#PWR?"  Part="1" 
F 0 "#PWR34" H 9900 2750 60  0001 C CNN
F 1 "GND" H 9900 2850 60  0000 C CNN
F 2 "" H 9900 3000 60  0000 C CNN
F 3 "" H 9900 3000 60  0000 C CNN
	1    9900 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR24
U 1 1 54E153B6
P 8200 3000
AR Path="/54E131DF/54E153B6" Ref="#PWR24"  Part="1" 
AR Path="/54E15959/54E153B6" Ref="#PWR?"  Part="1" 
F 0 "#PWR24" H 8200 2750 60  0001 C CNN
F 1 "GND" H 8200 2850 60  0000 C CNN
F 2 "" H 8200 3000 60  0000 C CNN
F 3 "" H 8200 3000 60  0000 C CNN
	1    8200 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR20
U 1 1 54E153BC
P 7300 3000
AR Path="/54E131DF/54E153BC" Ref="#PWR20"  Part="1" 
AR Path="/54E15959/54E153BC" Ref="#PWR?"  Part="1" 
F 0 "#PWR20" H 7300 2750 60  0001 C CNN
F 1 "GND" H 7300 2850 60  0000 C CNN
F 2 "" H 7300 3000 60  0000 C CNN
F 3 "" H 7300 3000 60  0000 C CNN
	1    7300 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2350 7300 3000
Wire Wire Line
	8200 2350 8200 3000
Wire Wire Line
	9000 2600 9000 3000
Wire Wire Line
	9300 2600 9300 3000
Wire Wire Line
	9600 2600 9600 3000
Wire Wire Line
	9900 2600 9900 3000
$Comp
L +BATT #PWR19
U 1 1 54E153C8
P 7300 1700
AR Path="/54E131DF/54E153C8" Ref="#PWR19"  Part="1" 
AR Path="/54E15959/54E153C8" Ref="#PWR?"  Part="1" 
F 0 "#PWR19" H 7300 1550 60  0001 C CNN
F 1 "+BATT" H 7300 1840 60  0000 C CNN
F 2 "" H 7300 1700 60  0000 C CNN
F 3 "" H 7300 1700 60  0000 C CNN
	1    7300 1700
	1    0    0    -1  
$EndComp
Connection ~ 7300 1850
$Comp
L PWR_FLAG #FLG6
U 1 1 54E153CF
P 9450 1800
AR Path="/54E131DF/54E153CF" Ref="#FLG6"  Part="1" 
AR Path="/54E15959/54E153CF" Ref="#FLG?"  Part="1" 
F 0 "#FLG6" H 9450 1895 30  0001 C CNN
F 1 "PWR_FLAG" H 9450 1980 30  0000 C CNN
F 2 "" H 9450 1800 60  0000 C CNN
F 3 "" H 9450 1800 60  0000 C CNN
	1    9450 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 1800 9450 1850
Connection ~ 9450 1850
$Comp
L VMOTOR #PWR33
U 1 1 54E153D7
P 9900 1700
AR Path="/54E131DF/54E153D7" Ref="#PWR33"  Part="1" 
AR Path="/54E15959/54E153D7" Ref="#PWR?"  Part="1" 
F 0 "#PWR33" H 9900 1550 60  0001 C CNN
F 1 "VMOTOR" H 9900 1840 60  0000 C CNN
F 2 "" H 9900 1700 60  0000 C CNN
F 3 "" H 9900 1700 60  0000 C CNN
	1    9900 1700
	1    0    0    -1  
$EndComp
Connection ~ 9900 1850
$Comp
L XC6210B302M U3
U 1 1 54E153DE
P 8200 3850
AR Path="/54E131DF/54E153DE" Ref="U3"  Part="1" 
AR Path="/54E15959/54E153DE" Ref="U?"  Part="1" 
F 0 "U3" H 8400 3350 60  0000 C CNN
F 1 "XC6210B302M" H 8200 3800 60  0000 C CNN
F 2 "" H 8200 3850 60  0000 C CNN
F 3 "" H 8200 3850 60  0000 C CNN
	1    8200 3850
	1    0    0    -1  
$EndComp
$Comp
L Cpol C18
U 1 1 54E153E5
P 7300 4550
AR Path="/54E131DF/54E153E5" Ref="C18"  Part="1" 
AR Path="/54E15959/54E153E5" Ref="C?"  Part="1" 
F 0 "C18" H 7300 4650 40  0000 L CNN
F 1 "2u2" H 7306 4465 40  0000 L CNN
F 2 "" H 7338 4400 30  0000 C CNN
F 3 "" H 7300 4550 60  0000 C CNN
	1    7300 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3850 7300 4000
Wire Wire Line
	7300 4000 7300 4350
Wire Wire Line
	7300 4000 7700 4000
$Comp
L Cpol C9
U 1 1 54E153EE
P 9000 4550
AR Path="/54E131DF/54E153EE" Ref="C9"  Part="1" 
AR Path="/54E15959/54E153EE" Ref="C?"  Part="1" 
F 0 "C9" H 9000 4650 40  0000 L CNN
F 1 "2u2" H 9006 4465 40  0000 L CNN
F 2 "" H 9038 4400 30  0000 C CNN
F 3 "" H 9000 4550 60  0000 C CNN
	1    9000 4550
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 54E153F5
P 9300 4550
AR Path="/54E131DF/54E153F5" Ref="C12"  Part="1" 
AR Path="/54E15959/54E153F5" Ref="C?"  Part="1" 
F 0 "C12" H 9350 4650 50  0000 L CNN
F 1 "10n" H 9350 4450 50  0000 L CNN
F 2 "" H 9338 4400 30  0000 C CNN
F 3 "" H 9300 4550 60  0000 C CNN
	1    9300 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 3850 9800 4000
Wire Wire Line
	9800 4000 9800 4050
Wire Wire Line
	9300 4000 9300 4350
Wire Wire Line
	9000 4000 9000 4350
Connection ~ 9000 4000
$Comp
L GND #PWR27
U 1 1 54E153FF
P 9000 5200
AR Path="/54E131DF/54E153FF" Ref="#PWR27"  Part="1" 
AR Path="/54E15959/54E153FF" Ref="#PWR?"  Part="1" 
F 0 "#PWR27" H 9000 4950 60  0001 C CNN
F 1 "GND" H 9000 5050 60  0000 C CNN
F 2 "" H 9000 5200 60  0000 C CNN
F 3 "" H 9000 5200 60  0000 C CNN
	1    9000 5200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR29
U 1 1 54E15405
P 9300 5200
AR Path="/54E131DF/54E15405" Ref="#PWR29"  Part="1" 
AR Path="/54E15959/54E15405" Ref="#PWR?"  Part="1" 
F 0 "#PWR29" H 9300 4950 60  0001 C CNN
F 1 "GND" H 9300 5050 60  0000 C CNN
F 2 "" H 9300 5200 60  0000 C CNN
F 3 "" H 9300 5200 60  0000 C CNN
	1    9300 5200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR25
U 1 1 54E1540B
P 8200 5200
AR Path="/54E131DF/54E1540B" Ref="#PWR25"  Part="1" 
AR Path="/54E15959/54E1540B" Ref="#PWR?"  Part="1" 
F 0 "#PWR25" H 8200 4950 60  0001 C CNN
F 1 "GND" H 8200 5050 60  0000 C CNN
F 2 "" H 8200 5200 60  0000 C CNN
F 3 "" H 8200 5200 60  0000 C CNN
	1    8200 5200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR22
U 1 1 54E15411
P 7300 5200
AR Path="/54E131DF/54E15411" Ref="#PWR22"  Part="1" 
AR Path="/54E15959/54E15411" Ref="#PWR?"  Part="1" 
F 0 "#PWR22" H 7300 4950 60  0001 C CNN
F 1 "GND" H 7300 5050 60  0000 C CNN
F 2 "" H 7300 5200 60  0000 C CNN
F 3 "" H 7300 5200 60  0000 C CNN
	1    7300 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 4500 7300 5200
Wire Wire Line
	8200 4500 8200 5200
Wire Wire Line
	9000 4750 9000 5200
Wire Wire Line
	9300 4750 9300 5200
$Comp
L +BATT #PWR21
U 1 1 54E1541B
P 7300 3850
AR Path="/54E131DF/54E1541B" Ref="#PWR21"  Part="1" 
AR Path="/54E15959/54E1541B" Ref="#PWR?"  Part="1" 
F 0 "#PWR21" H 7300 3700 60  0001 C CNN
F 1 "+BATT" H 7300 3990 60  0000 C CNN
F 2 "" H 7300 3850 60  0000 C CNN
F 3 "" H 7300 3850 60  0000 C CNN
	1    7300 3850
	1    0    0    -1  
$EndComp
Connection ~ 7300 4000
$Comp
L PWR_FLAG #FLG5
U 1 1 54E15422
P 8850 3950
AR Path="/54E131DF/54E15422" Ref="#FLG5"  Part="1" 
AR Path="/54E15959/54E15422" Ref="#FLG?"  Part="1" 
F 0 "#FLG5" H 8850 4045 30  0001 C CNN
F 1 "PWR_FLAG" H 8850 4130 30  0000 C CNN
F 2 "" H 8850 3950 60  0000 C CNN
F 3 "" H 8850 3950 60  0000 C CNN
	1    8850 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 4000 8850 4000
Wire Wire Line
	8850 4000 9000 4000
Wire Wire Line
	9000 4000 9300 4000
Wire Wire Line
	9300 4000 9800 4000
Wire Wire Line
	8850 3950 8850 4000
Connection ~ 8850 4000
$Comp
L VCC #PWR31
U 1 1 54E1542B
P 9800 3850
AR Path="/54E131DF/54E1542B" Ref="#PWR31"  Part="1" 
AR Path="/54E15959/54E1542B" Ref="#PWR?"  Part="1" 
F 0 "#PWR31" H 9800 3700 60  0001 C CNN
F 1 "VCC" H 9800 4000 60  0000 C CNN
F 2 "" H 9800 3850 60  0000 C CNN
F 3 "" H 9800 3850 60  0000 C CNN
	1    9800 3850
	1    0    0    -1  
$EndComp
Connection ~ 9300 4000
Wire Wire Line
	7700 2050 7550 2050
Wire Wire Line
	7550 2050 7550 3400
Wire Wire Line
	7550 3400 7550 4200
Wire Wire Line
	7550 4200 7550 4300
Wire Wire Line
	7550 3400 10150 3400
Text GLabel 10150 3400 2    60   Input ~ 0
PWREN
$Comp
L R R33
U 1 1 54E15438
P 7550 4550
AR Path="/54E131DF/54E15438" Ref="R33"  Part="1" 
AR Path="/54E15959/54E15438" Ref="R?"  Part="1" 
F 0 "R33" V 7630 4550 50  0000 C CNN
F 1 "10k" V 7557 4551 50  0000 C CNN
F 2 "" V 7480 4550 30  0000 C CNN
F 3 "" H 7550 4550 30  0000 C CNN
	1    7550 4550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR23
U 1 1 54E1543F
P 7550 5200
AR Path="/54E131DF/54E1543F" Ref="#PWR23"  Part="1" 
AR Path="/54E15959/54E1543F" Ref="#PWR?"  Part="1" 
F 0 "#PWR23" H 7550 4950 60  0001 C CNN
F 1 "GND" H 7550 5050 60  0000 C CNN
F 2 "" H 7550 5200 60  0000 C CNN
F 3 "" H 7550 5200 60  0000 C CNN
	1    7550 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 4800 7550 5200
Connection ~ 7550 3400
$Comp
L INDUCTOR_SMALL L1
U 1 1 54E15447
P 4700 5250
AR Path="/54E131DF/54E15447" Ref="L1"  Part="1" 
AR Path="/54E15959/54E15447" Ref="L?"  Part="1" 
F 0 "L1" H 4700 5350 50  0000 C CNN
F 1 "INDUCTOR_SMALL" H 4700 5200 50  0000 C CNN
F 2 "" H 4700 5250 60  0000 C CNN
F 3 "" H 4700 5250 60  0000 C CNN
	1    4700 5250
	1    0    0    -1  
$EndComp
$Comp
L C C16
U 1 1 54E1544E
P 5200 5800
AR Path="/54E131DF/54E1544E" Ref="C16"  Part="1" 
AR Path="/54E15959/54E1544E" Ref="C?"  Part="1" 
F 0 "C16" H 5250 5900 50  0000 L CNN
F 1 "100n" H 5250 5700 50  0000 L CNN
F 2 "" H 5238 5650 30  0000 C CNN
F 3 "" H 5200 5800 60  0000 C CNN
	1    5200 5800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR18
U 1 1 54E15455
P 5200 6300
AR Path="/54E131DF/54E15455" Ref="#PWR18"  Part="1" 
AR Path="/54E15959/54E15455" Ref="#PWR?"  Part="1" 
F 0 "#PWR18" H 5200 6050 60  0001 C CNN
F 1 "GND" H 5200 6150 60  0000 C CNN
F 2 "" H 5200 6300 60  0000 C CNN
F 3 "" H 5200 6300 60  0000 C CNN
	1    5200 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 6000 5200 6300
Wire Wire Line
	4950 5250 5000 5250
Wire Wire Line
	5000 5250 5200 5250
Wire Wire Line
	5200 5100 5200 5250
Wire Wire Line
	5200 5250 5200 5600
Wire Wire Line
	4200 5250 4200 5600
Connection ~ 4200 5250
$Comp
L AVDD #PWR17
U 1 1 54E15460
P 5200 5100
AR Path="/54E131DF/54E15460" Ref="#PWR17"  Part="1" 
AR Path="/54E15959/54E15460" Ref="#PWR?"  Part="1" 
F 0 "#PWR17" H 5200 4950 60  0001 C CNN
F 1 "AVDD" H 5200 5240 60  0000 C CNN
F 2 "" H 5200 5100 60  0000 C CNN
F 3 "" H 5200 5100 60  0000 C CNN
	1    5200 5100
	1    0    0    -1  
$EndComp
Connection ~ 5200 5250
$Comp
L R R37
U 1 1 54E4BB98
P 3300 3350
F 0 "R37" V 3380 3350 50  0000 C CNN
F 1 "25k" V 3307 3351 50  0000 C CNN
F 2 "" V 3230 3350 30  0000 C CNN
F 3 "" H 3300 3350 30  0000 C CNN
	1    3300 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 2150 3550 2150
$Comp
L +3V0 #PWR7
U 1 1 54E4BC95
P 3300 3050
F 0 "#PWR7" H 3300 2900 60  0001 C CNN
F 1 "+3V0" H 3300 3190 60  0000 C CNN
F 2 "" H 3300 3050 60  0000 C CNN
F 3 "" H 3300 3050 60  0000 C CNN
	1    3300 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 3100 3300 3050
Wire Wire Line
	3550 2150 3550 3600
Wire Wire Line
	3300 3600 3550 3600
Wire Wire Line
	3550 3600 5500 3600
Connection ~ 3550 3600
Text GLabel 5500 3600 2    60   Output ~ 0
CSTATE
$Comp
L R R21
U 1 1 54E7CC07
P 9800 4300
F 0 "R21" V 9880 4300 50  0000 C CNN
F 1 "20k" V 9807 4301 50  0000 C CNN
F 2 "" V 9730 4300 30  0000 C CNN
F 3 "" H 9800 4300 30  0000 C CNN
	1    9800 4300
	1    0    0    -1  
$EndComp
$Comp
L R R22
U 1 1 54E7CCEC
P 9800 4900
F 0 "R22" V 9880 4900 50  0000 C CNN
F 1 "2k2" V 9807 4901 50  0000 C CNN
F 2 "" V 9730 4900 30  0000 C CNN
F 3 "" H 9800 4900 30  0000 C CNN
	1    9800 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 4200 7550 4200
Connection ~ 7550 4200
$Comp
L GND #PWR32
U 1 1 54E7DF68
P 9800 5200
F 0 "#PWR32" H 9800 4950 60  0001 C CNN
F 1 "GND" H 9800 5050 60  0000 C CNN
F 2 "" H 9800 5200 60  0000 C CNN
F 3 "" H 9800 5200 60  0000 C CNN
	1    9800 5200
	1    0    0    -1  
$EndComp
Connection ~ 9800 4000
Wire Wire Line
	9800 4550 9800 4600
Wire Wire Line
	9800 4600 9800 4650
Wire Wire Line
	9800 5150 9800 5200
Text GLabel 10150 4600 2    60   Output ~ 0
VCCSENSE
Wire Wire Line
	9800 4600 10150 4600
Connection ~ 9800 4600
Text Notes 1550 1050 0    60   ~ 0
Battery charger
Text Notes 1550 4650 0    60   ~ 0
Processor digital and analog power
Text Notes 7750 1350 0    60   ~ 0
Switchable motor and comms power
$Comp
L PWR_FLAG #FLG4
U 1 1 54E802A4
P 5000 5200
F 0 "#FLG4" H 5000 5295 30  0001 C CNN
F 1 "PWR_FLAG" H 5000 5380 30  0000 C CNN
F 2 "" H 5000 5200 60  0000 C CNN
F 3 "" H 5000 5200 60  0000 C CNN
	1    5000 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5200 5000 5250
Connection ~ 5000 5250
$EndSCHEMATC
